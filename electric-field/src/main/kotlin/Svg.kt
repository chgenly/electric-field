import org.w3c.dom.HTMLDivElement
import org.w3c.dom.svg.SVGElement
import kotlin.browser.document
import kotlin.dom.appendText

class Svg(id: String, screenWidth: String, screenHeight: String, viewBoxWidth: Int, viewBoxHeight: Int) {
    val width = viewBoxWidth.toDouble()
    val height = viewBoxHeight.toDouble()
    private val svg : SVGElement
    private val ns = "http://www.w3.org/2000/svg"


    init {
        val div: HTMLDivElement = document.getElementById(id) as HTMLDivElement
        svg = document.createElementNS(ns, "svg") as SVGElement
        svg.setAttribute("viewBox", "0 0 $viewBoxWidth $viewBoxHeight")
        svg.setAttribute("width", screenWidth)
        svg.setAttribute("height", screenHeight)
        div.appendChild(svg)

        rect(Point(0.0, 0.0), Point(width, height), "#eeeeee")
    }

    fun rect(p0: Point, p1: Point, color: String) {
        val c = document.createElementNS(ns, "rect")
        c.setAttribute("x", "${p0.x.toInt()}")
        c.setAttribute("y", "${p0.y.toInt()}")
        c.setAttribute("width", "${p1.x - p0.x}")
        c.setAttribute("height", "${p1.y - p0.y}")
        c.setAttribute("fill", color)
        svg.appendChild(c)

    }

    fun text(center: Point, msg: String, color: String = BLACK) {
        val c = document.createElementNS(ns, "text")
        c.setAttribute("x", "${center.x.toInt()}")
        c.setAttribute("y", "${center.y.toInt()}")
        c.setAttribute("font-size", "120")
        c.setAttribute("fill", color)
        c.setAttribute("text-anchor", "middle")
        c.setAttribute("alignment-baseline", "central")
        c.setAttribute("font-weight", "bold")
        c.appendText(msg)
        svg.appendChild(c)
    }

    fun arrow(line: Line, width: Int = 20, color: String = BLACK) {
        line(line, color)
        arrowHead(line, width, color)
    }

    fun arrowHead(line: Line, width: Int = 20, color: String = BLACK) {
        val len = line.len()
        val a = Point(-line.dx() / len * width, -line.dy() / len * width)
        val left = a.rotate(PI_OVER_4).plus(line.p1)
        val right = a.rotate(-PI_OVER_4).plus(line.p1)
        val leftLine = Line(line.p1, left)
        val rightLine = Line(line.p1, right)
        line(leftLine, color)
        line(rightLine, color)
    }

    fun line(line: Line, color: String = BLACK) {
        val c = document.createElementNS(ns, "path")
        c.setAttribute("stroke", color)
        c.setAttribute("d", "M ${line.p0.x.toInt()} ${line.p0.y.toInt()} L ${line.p1.x.toInt()} ${line.p1.y.toInt()}")
        c.setAttribute("stroke-width", "5")
        svg.appendChild(c)
    }

    fun circle(center: Point, r: Double, color: String = BLACK) {
        val c = document.createElementNS(ns, "circle")
        c.setAttribute("cx", "${center.x.toInt()}")
        c.setAttribute("cy", "${center.y.toInt()}")
        c.setAttribute("r", "${r.toInt()}")
        c.setAttribute("fill", color)
        svg.appendChild(c)
    }

    fun spline(pts: List<Point>, color: String = BLACK) {
        val c = document.createElementNS(ns, "path")
        val sb = StringBuilder()
        var op = 'M'
        for(p in pts) {
            sb.append("$op ${p.x} ${p.y} ")
            op ='L'
        }
        c.setAttribute("d", sb.toString())
        c.setAttribute("stroke", color)
        c.setAttribute("fill", "transparent")
        svg.appendChild(c)
    }
}
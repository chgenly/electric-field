import kotlin.math.sqrt

data class Line(val p0: Point, val p1: Point) {
    fun dx(): Double {
        return p1.x - p0.x
    }

    fun dy(): Double {
        return p1.y - p0.y
    }

    fun len(): Double {
        val dx: Double = dx()
        val dy: Double = dy()
        return sqrt(dx * dx + dy * dy)
    }

    fun swap(): Line {
        return Line(p1, p0)
    }
}
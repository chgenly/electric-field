import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

data class Point(val x: Double, val y: Double) {
    fun rotate(angle: Double): Point {
        val sin = sin(angle)
        val cos = cos(angle)
        return Point(x * cos - y * sin, x * sin + y * cos)
    }

    operator fun plus(p: Point): Point {
        return Point(p.x + x, p.y + y)
    }

    operator fun minus(p: Point): Point {
        return Point(x - p.x, y - p.y)
    }

    operator fun times(d: Double): Point {
        return Point(x * d, y * d)
    }

    operator fun div(d: Double): Point {
        return Point(x / d, y / d)
    }

    fun distance(p: Point): Double {
        return (this-p).length()
    }

    fun length(): Double {
        return sqrt(x * x + y * y)
    }

    fun normalize(): Point {
        return this / length()
    }

    operator fun unaryMinus(): Point = Point(-x, -y)
}
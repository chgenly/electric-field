import kotlin.math.PI

val RED = "#ff0000"
val GREEN = "#00ff00"
val BLACK = "#000000"
val PI_OVER_2 = PI / 2
val PI_OVER_4 = PI / 4

class Diagrams {

    val chargeR = 80.0

    @JsName("positive")
    fun positive(id: String, w: String, h: String, viewBoxWidth: Int, viewBoxHeight: Int) {
        val svg1 = Svg(id, w, h, viewBoxWidth, viewBoxHeight)
        oneCharge(svg1, true)
    }

    @JsName("negative")
    fun negative(id: String, w: String, h: String, viewBoxWidth: Int, viewBoxHeight: Int) {
        val svg1 = Svg(id, w, h, viewBoxWidth, viewBoxHeight)
        oneCharge(svg1, false)
    }

    @JsName("positiveAndNegative")
    fun positiveAndNegative(id: String, w: String, h: String, viewBoxWidth: Int, viewBoxHeight: Int) {
        val svg1 = Svg(id, w, h, viewBoxWidth, viewBoxHeight)
        twoCharges(svg1)
    }

    fun twoCharges(svg: Svg) {
        val DELTA = 10.0
        fun force(p: Point, chargeCenter: Point): Point {
            val d = p.distance(chargeCenter)
            return (p - chargeCenter) / d / d
        }

        fun force(p: Point, posCenter: Point, negCenter: Point): Point {
            return force(p, posCenter) - force(p, negCenter)
        }

        val posCenter = Point(svg.width * 1 / 4, svg.height / 2)
        val negCenter = Point(svg.width * 3 / 4, svg.height / 2)

        positiveCharge(svg, posCenter, chargeR)
        negativeCharge(svg, negCenter, chargeR)

        val spokeCnt = 32
        for (spoke in 0 until spokeCnt) {
            val pts = mutableListOf<Point>()
            val angle = 2 * PI * spoke / spokeCnt
            var p = posCenter + Point(0.0, chargeR).rotate(angle)
            var i = 0
            while (true) {
                val f = force(p, posCenter, negCenter)
                val nf = f.normalize()
                pts.add(p)
                if (p.x >= svg.width / 2) break
                p = p + nf.normalize() * DELTA
                ++i
                if (i > 1000) break
            }
            svg.spline(pts)
            val mpts = mutableListOf<Point>()
            for (pt in pts) {
                val mpt = -(pt - posCenter) + negCenter
                mpts.add(mpt)
            }
            svg.spline(mpts)
            val pn = pts[pts.size - 1]
            val pnm1 = pts[pts.size - 2]
            svg.arrowHead(Line(pnm1, pn))
        }
    }

    fun oneCharge(svg: Svg, positive: Boolean) {
        val chargeCenter = Point(svg.width / 2, svg.height / 2)

        if (positive)
            positiveCharge(svg, chargeCenter, chargeR)
        else
            negativeCharge(svg, chargeCenter, chargeR)

        val D0 = chargeR * 1.40
        var d = D0
        /**
         * Scale the strength so it has a good length on the plot
         */
        val STRENGTH_SCALE = 2500
        /**
         * Plot strengths for a given point at a point this much
         * closer to the charge center.  This is to put us on a nice
         * part of the inverse square law curve so we can see more than
         * one arrow.  If this were set to zero, the plot would be
         * completely realistic.
         */
        val D_OFFSET = 250.0
        val DOT_R = chargeR /10
        for (i in 0..2) {
            val strength = D0 * D0 * STRENGTH_SCALE / (d + D_OFFSET) / (d + D_OFFSET)
            val p0 = Point(d, 0.0)
            val v0 = Point(d + DOT_R, 0.0)
            val v1 = Point(d + DOT_R + strength, 0.0)
            val CIRCLE_SLICES = 20
            for (a in 0..CIRCLE_SLICES) {
                val rp0 = p0.rotate(a * 2 * PI / CIRCLE_SLICES).plus(chargeCenter)
                val rv0 = v0.rotate(a * 2 * PI / CIRCLE_SLICES).plus(chargeCenter)
                val rv1 = v1.rotate(a * 2 * PI / CIRCLE_SLICES).plus(chargeCenter)
                val line = Line(rv0, rv1)

                svg.circle(rp0, DOT_R)
                svg.arrow(if (positive) line else line.swap())
            }
            d += strength + 2*DOT_R + chargeR / 4
        }
    }

    fun positiveCharge(svg: Svg, chargeCenter: Point, chargeR: Double) {
        svg.circle(chargeCenter, chargeR, RED)

        val cx = chargeCenter.x
        val cy = chargeCenter.y
        val w2 = chargeR * .8 / 2

        val horiz = Line(Point(cx - w2, cy), Point(cx + w2, cy))
        svg.line(horiz)

        val vert = Line(Point(cx, cy - w2), Point(cx, cy + w2))
        svg.line(vert)
    }

    fun negativeCharge(svg: Svg, chargeCenter: Point, chargeR: Double) {
        svg.circle(chargeCenter, chargeR, GREEN)

        val cx = chargeCenter.x
        val cy = chargeCenter.y
        val w2 = chargeR * .8 / 2

        val horiz = Line(Point(cx - w2, cy), Point(cx + w2, cy))
        svg.line(horiz)
    }
}
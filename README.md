# README #

These tools are used to draw electric field diagrams for the blog post at https://chgenly.wordpress.com/2017/12/24/wimshurst-electrostatic-generator/.
The diagrams are drawn using svg elements in a web page.

This is an IntelliJ kotlin project

### How do I get set up? ###

* clone the project to your local machine
* open IntelliJ, and open this project
* Build it: cntrl-F9
* open index.html, press one of the browser icons that appears in the upper right hand of the editor.

A web page should be shown in the browser with the diagrams.